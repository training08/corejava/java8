package com.adv.optionalex;

import java.lang.ref.WeakReference;
import java.util.Optional;

public class TestOptional {

	public static void main(String[] args) {
		Student student = new Student("tiger", 10);
		//System.out.println(student);
//		if (student != null) {
//		}
		
		//WeakReference<Student> weakReference = new WeakReference<Student>(student);
//		while (true) {
//			System.out.println(weakReference.get());
//		}
		
//		Optional<Student> stdOptional = Optional.of(null);
//		Optional<Student> stdOptional1 = Optional.ofNullable(null);
		
		//student = null;
		//Optional<Student> studentOptional = Optional.of(student);
		Optional<Student> studentOptional1 = Optional.ofNullable(student);
		//Student studentObj = Optional.ofNullable(student).orElse(new Student("TestNormal", 10));
		//Student studentObj1 = Optional.ofNullable(student).orElseGet(() -> new Student("TestGet", 10));
		
		//Student studentObj1 = Optional.ofNullable(student).orElseThrow();
		//Student studentObj1 = Optional.ofNullable(student).orElseThrow(() -> new ObjectNullException());
		
		//System.out.println("studentObj -> "+studentObj);
		//System.out.println("studentObj1 -> "+studentObj1);
		
		Student stdOptVal = studentOptional1.filter((std) -> {
			System.out.println("Inside filter");
			return std.getAge() > 10;
		}).orElseGet(() -> new Student("Age Less than 15", 15));
		System.out.println("Student Filter --->"+stdOptVal);
		
//		if (!studentOptional1.isEmpty()) {
//			Student std = studentOptional1.get();
//			System.out.println("student obj="+std);
//		}
		
//		studentOptional1.ifPresent((obj) -> {
//			System.out.println("student obj="+obj);	
//		});
		
		//FlatMap
		//studentOptional1.flatMap((std)-> std);
		
	}
}

class Student {
	
	private String name;
	
	private int age;

	public Student(String name, int age) {
		super();
		System.out.println("constructor invoked...........");
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + "]";
	}
}

class ObjectNullException extends RuntimeException {
	
}