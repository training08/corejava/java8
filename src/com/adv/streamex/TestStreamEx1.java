package com.adv.streamex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestStreamEx1 {

	public static void main(String[] args) {
		List<Emp> empList = new ArrayList<>();
		empList.add(new Emp("Tiger", 17, "XII", "t1@gmail.com"));
		empList.add(new Emp("Lion", 16, "XI", "liongmail.com"));
		empList.add(new Emp("Tiger", 17, "XII", "t1@gmail.com"));
		empList.add(new Emp("Zebra", 17, "XII", "zebra@gmail.com"));
		empList.add(new Emp("Cat", 18, "XII", "cat@gmail.com"));
		empList.add(new Emp("Cow", 17, "XII", "cow@gmail.com"));
		empList.add(new Emp("Fox", 12, "VII", "fox@gmail.com"));
		empList.add(new Emp("Cat", 17, "XII", "cat@gmail.com"));
		empList.add(new Emp("Zebra", 13, "VII", "zebra@gmail.com"));
		empList.add(new Emp("Elephant", 16, "XI", "elephant@gmail.com"));
		empList.add(new Emp("Cub", 16, "XII", "cub@gmail.com"));
		empList.add(new Emp("Dog", 19, "XII", "dog@gmail.com"));
		empList.add(new Emp("Tiger", 15, "X", "t1@gmail.com"));
		
		Set<Emp> empSet = empList.stream().distinct().filter(e -> e.getStd().equals("XII")).filter(e -> e.getAge() > 16).collect(Collectors.toSet());
		System.out.println("empSet="+empSet);
		
		Optional<Emp> empOptional = empList.stream().filter(e -> e.getStd().equals("XII")).findFirst();
		empOptional.ifPresent((e)-> System.out.println(e));
		
		Set<String> empNameSet = empList.stream().filter(e -> e.getStd().equals("XII")).map(x -> x.getName()).collect(Collectors.toSet());
		System.out.println("Emp Name Set="+empNameSet);
		
		List<Emp> empList1 = new ArrayList<>();
		empList1.add(new Emp("Tiger", 17, "XII", "t1@gmail.com", new ArrayList<String>(){{add("blr");add("kgi1");}}));
		empList1.add(new Emp("Lion", 16, "XI", "liongmail.com", new ArrayList<String>(){{add("ap");add("gj");}}));
		empList1.add(new Emp("Tiger", 17, "XII", "t1@gmail.com", new ArrayList<String>(){{add("mp");add("dl");}}));
		
		List<String> empAddressList = empList1.stream().filter(e -> e.getStd().equals("XII")).flatMap(x -> x.getAddress().stream()).collect(Collectors.toList());
		
		empList1.stream().filter(e -> e.getStd().equals("XII")).flatMap(x -> x.getAddress().stream()).forEach(System.out::println);
		
		Optional<String> strOpt = empList1.stream().filter(e -> e.getStd().equals("XII")).flatMap(x -> x.getAddress().stream()).reduce((e1, e2) -> e1.length()>(e2.length()) ? e1 : e2);
		
		strOpt.ifPresent(e -> System.out.println("The value from reduce method = "+e));
		
		System.out.println("empAddressList="+empAddressList);
		
		//empAddressList.forEach(e -> System.out.println(e));
		
		
		empList.forEach(abcd -> {
			String name = abcd.getName();
			System.out.println("name="+name);
		});
		
	}
}

class Emp {
	
	private String name;
	
	private int age;
	
	private String std;
	
	private String email;
	
	private List<String> address;

	public Emp(String name, int age, String std, String email) {
		super();
		this.name = name;
		this.age = age;
		this.std = std;
		this.email = email;
	}
	
	public Emp(String name, int age, String std, String email, List<String> address) {
		super();
		this.name = name;
		this.age = age;
		this.std = std;
		this.email = email;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getStd() {
		return std;
	}

	public void setStd(String std) {
		this.std = std;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public List<String> getAddress() {
		return address;
	}

	public void setAddress(List<String> address) {
		this.address = address;
	}
	
	@Override
	public int hashCode() {
		return (this.name+this.std).hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		System.out.println("Inside equals..........");
		Emp empObj = (Emp) obj;
		return (this.name+this.std).equals(empObj.getName()+empObj.getStd());
	}

	@Override
	public String toString() {
		return "Emp [name=" + name + ", age=" + age + ", std=" + std + ", email=" + email + ", address=" + address
				+ "]";
	}
}