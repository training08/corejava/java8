package com.adv.streamex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TestStream {

	public static void main(String[] args) {
		
		int a[] = {5,10,2,3,4,11,16,7};
		boolean value = Arrays.stream(a).anyMatch(e -> e == 10);
		System.out.println("The value 10 present in array ="+value);
		
		String str1[] = {"Tiger", "Lion", "Zebra", "Tiger", "Cat"};
		List<String> strValList = Arrays.stream(str1).filter(e -> !e.equals("Tiger")).collect(Collectors.toList());
		System.out.println("strValList="+strValList);
		
		
		List<String> strList = new ArrayList<>();
		strList.add("A");
		strList.add("B");
		strList.add("C");
		strList.add("B");
		strList.add("X");
		strList.add("E");
		strList.add("A");
		strList.add("A");

//		for (int i = 0; i < strList.size(); i++) {
//			System.out.println(strList.get(i));
//		}
		
		int count = 0;
		for (String str : strList) {
			//System.out.println("Value="+str);
			if (str.equals("A")) {
				count++;
			}
		}
		System.out.println("The count of A = "+count);
	
		//filtering
		long countVal = strList.stream().filter((e)-> e.equals("A")).count();
		System.out.println("The count of A using stream = "+countVal);
	}
}
