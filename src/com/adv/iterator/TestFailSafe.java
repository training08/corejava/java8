package com.adv.iterator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TestFailSafe {

	public static void main(String[] args) {
		//Map<String, String> mp = new ConcurrentHashMap<>();
		Map<String, String> mp = new HashMap<>();
		mp.put("tiger", "t1");
		mp.put("zebra", "z1");
		mp.put("lion", "l1");
		
		Set<Entry<String, String>> entrySet = mp.entrySet();
		Iterator<Entry<String, String>> entryIter = entrySet.iterator();
		while (entryIter.hasNext()) {
			Entry<String, String> entry = entryIter.next();
			String key = entry.getKey();
			String value = entry.getValue();
			System.out.println("Key="+key+", value="+value);
			
			mp.put("cat", "c1");
		}
	}
}
