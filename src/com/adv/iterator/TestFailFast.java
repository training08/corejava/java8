package com.adv.iterator;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class TestFailFast {

	public static void main(String[] args) {
		//List<String> strList = new ArrayList<>();
		List<String> strList = new CopyOnWriteArrayList<>();
		strList.add("tiger");
		strList.add("lion");
		strList.add("zebra");
		
		Iterator<String> strIter = strList.iterator();
		while (strIter.hasNext()) {
			System.out.println(strIter.next());
			strList.add("cow");
		}
	}
}
