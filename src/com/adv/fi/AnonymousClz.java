package com.adv.fi;

public class AnonymousClz {

	public static void main(String[] args) {
		
		MyFI myFI = new MyFiImpl();
		
		MyFI myFI1 = new MyFI() {
			
			@Override
			public void m1() {
				System.out.println("Inside m1 - Anonymous");
			}
		};
		
		myFI1.m1();
		
		
		MyFiImpl myFiImpl = new MyFiImpl() {

			@Override
			public void m1() {
				System.out.println("Inside myFiImpl - Anonymous Impl");
			}
		};
		
		MyFiImpl myFiImpl1 = new MyFiImpl() {

			@Override
			public void m1() {
				
			}
		};
		
		MyFI myFI2 = () -> {
			System.out.println("Inside myFiImpl - Anonymous Impl");
		};
		
		
	}
}
