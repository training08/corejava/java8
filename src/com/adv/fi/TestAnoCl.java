package com.adv.fi;

public class TestAnoCl {

	public static void main(String[] args) {
		Sample sample = new Sample();
		int addResult = sample.add(5, 4);
		System.out.println("addition result="+addResult);
		int subResult = sample.sub(4, 5);
		System.out.println("subraction result="+subResult);
		
		Sample sample2 = new Sample() {
			
			@Override
			public int sub(int a, int b) {
				if (a < b) {
					return (a+b) - b;
				}
				return (a - b);
			}
		};
		int newSubRes = sample2.sub(4, 5);
		System.out.println("subraction result="+newSubRes);
		
		subResult = sample.sub(4, 5);
		System.out.println("subraction result="+subResult);
	}
}
