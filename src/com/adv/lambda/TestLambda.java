package com.adv.lambda;

public class TestLambda {

	public static void main(String[] args) {
		Fi1 fi1 = new Fi1() {
			
			@Override
			public boolean compareEqual(int a, int b) {
				if (a == b) 
					return true;
				return false;
			}
		};
		
		Fi1 fi11 = (a, b) ->  {
			if (a == b) 
				return true;
			return false;
		};
		
		Fi1 fi111 = (a, b) -> a == b;
		
		System.out.println(fi11.compareEqual(5, 8));
		
		
//		Fi2 fi2 = (a, b) -> {return a+b;};
		Fi2 fi2 = (a, b) -> a+b;
		System.out.println(fi2.add(5, 5));
	}
}
